<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'smilecare');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'root');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '@FgP;LWl24MCBFIWa;#8BINFQ2b.S<?{[%X&@9@`yHp7i1&@8St=.}1?2h]muFyh');
define('SECURE_AUTH_KEY',  '?r{-:LvK,ue7:9c`4IC]ae1,HJ(#Zy)iML-Y<TN}WtuTiR*K `!)sS2zsR.q.Q5C');
define('LOGGED_IN_KEY',    'Bext([FmIjWaK3s;Syewrq~FC8LN#NyuD>5Tle71U-*H*n6ko_,9b1c-O8IT!UyB');
define('NONCE_KEY',        '%m5:.c?LLI~%8C,ZQhVwi,zTSD#VB~ B2uQSlAXILv^,NHY+ZI~qaXfx_;:+iVDT');
define('AUTH_SALT',        'H$ZOH}w}t>N*A8-5neSqHFz<*V#NZF,s:k=+THFf=[w-|HcBO%lvT&V*lixp{gFq');
define('SECURE_AUTH_SALT', 'h]+>;*$sq?x3I2E]L;:_:N)dT>SL.8)EX)WV9H,}b PR>A/:;~6FV{9n:u;iSQO@');
define('LOGGED_IN_SALT',   'HxqrTOgOINz|126RxxCo(p,9*&A&%:.SG_4ilM>F3EkH@! ye@Rm<Hl_(s:c/^&j');
define('NONCE_SALT',       '~QLogdHtS@|gk0YLw]I0@[L<az$OwFwaC*}:| rp(zIn.p4F}?aB_;`,01~B}^y`');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
