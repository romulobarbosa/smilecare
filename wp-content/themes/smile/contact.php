<?php
/*
  Template Name: Contato
*/
?>

<?php include '_header.php'; ?>

  <section class="internal-banner" style="background-image: url(<?php echo TPATH; ?>/images/contact-bg.png)"></section>

  <main class="contact">
    <div class="wrapper">
      <span class="section-name">Contato</span>

      <div class="contact-text">
        <?php
        while ( have_posts() ) : the_post();
          the_content();
        endwhile;
        ?>

      </div>
    </div>
  </main>

<?php include '_footer.php'; ?>
