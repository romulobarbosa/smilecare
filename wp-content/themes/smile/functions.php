<?php

define( 'WP_DEBUG', true );

/**
* Stores the template directory path for easy access
*/
define('TPATH', get_bloginfo('template_directory'));

// wpcf7
add_filter( 'wpcf7_validate_configuration', '__return_false' );

/**
* Post thumbnails
*/
add_theme_support( 'post-thumbnails' );

/**
* Menu
*/
function register_my_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_my_menu' );

/**
* Remove text editor from comunity page
*/
add_action( 'admin_init', 'hide_editor' );
function hide_editor() {
        $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
        if( !isset( $post_id ) ) return;
        $template_file = get_post_meta($post_id, '_wp_page_template', true);
    if($template_file == 'infos.php'){ // edit the template name
        remove_post_type_support('page', 'editor');
    }
}


/**
* WP Login - chenge logo
*/
function my_login_logo() { ?>
  <style type="text/css">
  #login h1 a, .login h1 a {
    background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png);
    height:79px;
    width:192px;
    -webkit-background-size: cover;
    background-size: cover;
    background-repeat: no-repeat;
    margin-bottom: 30px;
  }
  #login .input:focus {
    border-color: #51BDBC;
    box-shadow: 0 0 2px rgba(91, 157, 217, .8)
  }
  #login #wp-submit {
    background-color: #51BDBC;
    background: #51BDBC;
    border-color: #51BDBC #51BDBC #51BDBC;
    box-shadow: 0 1px 0 #51BDBC;
    text-shadow: 0 -1px 1px #51BDBC, 1px 0 1px #51BDBC, 0 1px 1px #51BDBC, -1px 0 1px #51BDBC;
  }
</style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

function my_login_logo_url() {
  return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
  return 'Smile Care';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );

// Language Selector
function hot_language_selector() {
  if (function_exists('icl_get_languages')) {
    $languages = icl_get_languages('skip_missing=0&orderby=code');

    if(!empty($languages)){
      foreach($languages as $l) {
        if($l['active']) {
          echo '<li class="menu-item active">'.$l['native_name'].'</li>';
        } else {
          echo '<li class="menu-item"><a href="'.$l['url'].'">';
        echo $l['native_name'];
        echo '</li></a>';
        }
      }
    }
  }
}


?>
