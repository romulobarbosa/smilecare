var gulp = require('gulp');
var sass = require('gulp-sass');
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var minifyCSS = require('gulp-minify-css');
var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');
var del = require('del');
var runSequence = require('run-sequence');
var autoprefixer = require('gulp-autoprefixer');

// Gulp Sass
gulp.task('sass', function() {
  return gulp.src('scss/**/*.scss')
    .pipe(sass())
    .pipe(autoprefixer({
      browsers: ['> 1%', 'last 2 versions']
      }))
    .pipe(gulp.dest('./css'))
});

// Gulp Watch
gulp.task('watch', ['sass'], function(){
  gulp.watch('scss/**/*.scss', ['sass']);
});

// Gulp default
gulp.task('serve', function (callback) {
  runSequence(['sass', 'watch'],
    callback
  )
});

// Gulp default
gulp.task('default', function (callback) {
  runSequence('serve');
})
