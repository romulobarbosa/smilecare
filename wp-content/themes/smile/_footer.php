<footer class="footer">
  <?php
  $infos = new WP_Query('pagename=home');
  if ( $infos->have_posts() ) :
    $infos->the_post();
    ?>
    <div class="wrapper">

      <div class="footer-column">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="footer-logo"><img src="<?php echo TPATH; ?>/images/logo-white.png" alt=""></a>
        <span class="footer-rights">&copy; 2018 Smile Care.<br>&copy; <a href="https://marcuscastro.portfoliobox.net" target="_blank">Imaginar</a></span>
      </div>
      <div class="footer-column">
        <h4 class="footer-title">Atendimento</h4>
        <p>Av Maria Lamas, 6 - A<br>Serra das Minas<br>Lisbon, Portugal . 2635-430</p>
        <p><?php echo get_field('footer_hour')?></p>

      </div>
      <div class="footer-column">
        <h4 class="footer-title">Contate-nos</h4>
        <p>
          <?php echo get_field('footer_phones')?><br>
          <?php if(get_field('footer_email')) : ?>
            <a href="mailto:<?php echo get_field('footer_email')?>"><?php echo get_field('footer_email')?></a>
            <?php endif ?>
        </p>
      </div>
      <div class="footer-column">
        <h4 class="footer-title">Siga-nos</h4>
        <p><a href="http://instagram.com/smilecarept" target="_blank">Instagram</a><br><a href="http://facebook.com/smilecarept" target="_blank">Facebook</a></p>
      </div>
    </div>
    <?php
  endif;
  wp_reset_query();
  ?>
</footer>
</body>
</html>
