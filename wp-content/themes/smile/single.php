<?php
/*
  Template Name: Blog
*/
?>

<?php include '_header.php'; ?>

<?php
  function the_excerpt_max_charlength($charlength) {
    $excerpt = get_the_excerpt();
    $charlength++;

    if ( mb_strlen( $excerpt ) > $charlength ) {
      $subex = mb_substr( $excerpt, 0, $charlength - 5 );
      $exwords = explode( ' ', $subex );
      $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
      if ( $excut < 0 ) {
        echo mb_substr( $subex, 0, $excut );
      } else {
        echo $subex;
      }
      echo '...';
    } else {
      echo $excerpt;
    }
  }
?>

<?php
the_post();
global $postID;
$postID = get_the_ID();
?>

  <section class="internal-banner" style="background-image: url(<?php echo TPATH ?>/images/blog-bg.png)"></section>

  <main class="blog">
    <div class="wrapper">
      <span class="section-name">BLOG</span>
      <div class="post">
        <div class="post-content">
          <span class="post-date"><?php echo get_the_date(); ?></span>
          <h2 class="title post-title"><?php the_title(); ?></h2>
          <?php the_content(); ?>
        </div>
        <div class="post-share">
          <a href="https://www.facebook.com/sharer/sharer.php?u=#http://google.com" target="_blank" class="post-share-item"><img src="<?php echo TPATH ?>/images/post-facebook.png" alt=""></a>
          <a href="http://twitter.com/share?text=Veja &url=http://google.com" target="_blank" class="post-share-item"><img src="<?php echo TPATH ?>/images/post-twitter.png" alt=""></a>
        </div>
      </div>
    </div>

    <div class="wrapper more-posts">
      <h4 class="title title-secondary more-posts-title">Mais Posts</h3>
      <div class="posts-list">

        <?php
        wp_reset_query();
        $posts = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page' => '-1'));

        if ( $posts->have_posts() ) :
          while ( $posts->have_posts() ) : $posts->the_post();
            if ($postID != get_the_ID()) :
              ?>

              <a href="<?php the_permalink(); ?>" class="posts-list-item">
                <div class="posts-list-img" style="background-image: url(<?php the_post_thumbnail_url('full'); ?>)"></div>
                <div class="posts-list-content">
                  <span class="posts-list-date"><?php echo get_the_date(); ?></span>
                  <h5 class="posts-list-title"><?php the_title(); ?></h5>
                  <p class="posts-list-text"><?php echo the_excerpt_max_charlength(100); ?></p>
                </div>
              </a>

              <?php
            endif;
          endwhile;
          wp_reset_postdata();
        endif;
        ?>

      </div>
    </div>
  </main>

<?php include '_footer.php'; ?>
