<?php
/*
  Template Name: Especialidades
*/
?>

<?php include '_header.php'; ?>

  <section class="internal-banner" style="background-image: url(<?php echo TPATH ?>/images/specialties-bg.png)"></section>

  <main class="specialties">
    <div class="wrapper">
      <span class="section-name"><?php echo get_field('specialties_tag'); ?></span>

      <div class="specialties-list">

        <?php
        $specialties = new WP_Query(array('post_type'=>'specialtie', 'post_status'=>'publish', 'posts_per_page' => '-1'));
        if ( $specialties->have_posts() ) :
          while ( $specialties->have_posts() ) : $specialties->the_post();
            ?>
            <div class="specialtie-item">
              <h3 class="specialtie-title"><?php the_title(); ?></h3>
              <p class="specialtie-text"><?php echo get_field('text'); ?></p>
              <a href="#" class="specialtie-link"><?= __('Make an appointment', 'Smilecare'); ?></a>
            </div>
            <?php
          endwhile;
          wp_reset_postdata();
        endif;
        ?>

      </div>
    </div>
  </main>

<?php include '_footer.php'; ?>
