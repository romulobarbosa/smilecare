<!DOCTYPE html>
<html lang="pt-BR">
<head>
  <meta charset="UTF-8">
  <!--[if IE]><![endif]-->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>SmileCare</title>
  <meta name="author" content="Rômulo Barbosa" />
  <link rel="shortcut icon" href="favicon.ico" />
  <link href="https://fonts.googleapis.com/css?family=Roboto|Montserrat:300,700" rel="stylesheet">

  <!--build:css css/styles.min.css-->
  <link rel="stylesheet" href="<?php echo TPATH; ?>/css/styles.css">
  <!--endbuild-->

  <!--build:js js/main.min.js-->
  <script type="text/javascript" src="<?php echo TPATH; ?>/js/vendor/jquery-1.11.1.min.js"></script>
  <script type="text/javascript" src="<?php echo TPATH; ?>/js/vendor/tether.js"></script>
  <script type="text/javascript" src="<?php echo TPATH; ?>/js/vendor/bootstrap.js"></script>
  <script type="text/javascript" src="<?php echo TPATH; ?>/js/vendor/carousel.js"></script>
  <script type="text/javascript" src="<?php echo TPATH; ?>/js/main.js"></script>
  <!--endbuild-->

  <!--[if IE 8]>
  <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>
<body>

  <!--[if lt IE 10]>
    <p class="browserupgrade">Você está utilizando um <strong>navegador</strong> antigo. Para sua segurança, <a href="http://browsehappy.com/">atualize-o</a> e melhore sua experiência.</p>
  <![endif]-->

  <!-- ================================== -->
  <!-- header -->
  <!-- ================================== -->
  <header id="header" class="header">
    <div class="wrapper">
      <nav class="menu">
        <div class="logo">
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo TPATH; ?>/images/logo.png" alt=""></a>
        </div>

        <a href="#" class="menu-btn menu-open"><img src="<?php echo TPATH; ?>/images/menu-open.png" alt=""></a>

        <div class="menu-list">
          <a href="#" class="menu-btn menu-close"><img src="<?php echo TPATH; ?>/images/menu-close.png" alt=""></a>

          <ul class="menu-pages">
            <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
          </ul>


          <ul class="menu-languages">
            <?php hot_language_selector(); ?>
          </ul>

          <a href="<?php echo esc_url( home_url( '/contato' ) ); ?>" class="menu-contact btn">Contate-nos</a>
        </div>
      </nav>
    </div>
  </header>
