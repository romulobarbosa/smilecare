<?php include '_header.php'; ?>

  <section class="banner-container owl-carousel">
    <?php
    $banners = new WP_Query(array('post_type'=>'banner_home', 'post_status'=>'publish'));
      if ( $banners->have_posts() ) :
        while ( $banners->have_posts() ) : $banners->the_post();
          ?>
          <div class="banner" style="background-image: url('<?php the_post_thumbnail_url('full'); ?>')">
            <div class="wrapper">
              <div class="banner-content">
                <h2 class="title banner-title"><?php the_title(); ?></h2>
                <p class="banner-text"><?php echo get_field('texto'); ?></p>
                <a href="<?php echo get_field('link'); ?>" target="_blank" class="btn btn-white banner-btn">Ver mais</a>
              </div>
            </div>
          </div>
          <?php
        endwhile;
        wp_reset_postdata();
      endif;
      ?>
  </section>

  <section class="home-blog">
    <div class="wrapper">

    <?php
      wp_reset_postdata();
      $posts = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page' => '1'));

      if ( $posts->have_posts() ) :
        while ( $posts->have_posts() ) : $posts->the_post();
          ?>

          <?php
          function the_excerpt_max_charlength($charlength) {
            $excerpt = get_the_excerpt();
            $charlength++;

            if ( mb_strlen( $excerpt ) > $charlength ) {
              $subex = mb_substr( $excerpt, 0, $charlength - 5 );
              $exwords = explode( ' ', $subex );
              $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
              if ( $excut < 0 ) {
                echo mb_substr( $subex, 0, $excut );
              } else {
                echo $subex;
              }
              echo '...';
            } else {
              echo $excerpt;
            }
          }
          ?>

          <div class="home-blog-bg">
            <img src="<?php the_post_thumbnail_url('full'); ?>" alt="">
          </div>
          <div class="home-blog-content">
            <span class="section-name">BLOG</span>
            <h2 class="title title-secondary"><?php the_title(); ?></h2>
            <p class="home-blog-text"><?php echo the_excerpt_max_charlength(240); ?></p>
            <a href="<?php the_permalink(); ?>" class="btn"><?= __('Read More', 'Smilecare'); ?></a>
          </div>

          <?php
        endwhile;
        wp_reset_query();
        wp_reset_postdata();
      endif;
      ?>
    </div>
  </section>

  <section class="home-promotions">
    <div class="wrapper">
      <?php
      $infos = new WP_Query('pagename=home');
      if ( $infos->have_posts() ) :
        $infos->the_post();
        ?>
        <?php if (get_field('home_promotions_tag')) :?>
          <span class="section-name"><?php echo get_field('home_promotions_tag'); ?></span>
        <?php endif;?>
        <h2 class="title title-secondary">
          <?php echo get_field('home_promotions'); ?>
        </h2>
        <?php
      endif;
      wp_reset_query();
      ?>
    </div>

    <div class="wrapper">
      <div class="home-promotions-content">
        <?php
        $promotions = new WP_Query(array('post_type'=>'promotions', 'post_status'=>'publish'));
        if ( $promotions->have_posts() ) :
          $count = 1;
          while ( $promotions->have_posts() ) : $promotions->the_post();
            ?>
            <div class="home-promotion">
              <span class="home-promotion-num"><?php echo $count; ?></span>
              <p class="home-promotion-text"><?php echo get_field('text') ?></p>
              <a href="<?php echo get_field('link') ?>" class="home-promotion-link">Saber Mais</a>
            </div>
            <?php
            $count = $count + 1;
          endwhile;
          wp_reset_postdata();
        endif;
        ?>
      </div>
    </div>
  </section>

  <?php
  $video = new WP_Query(array('post_type'=>'video', 'post_status'=>'publish', 'posts_per_page' => '1'));
  if ( $video->have_posts() ) :
    while ( $video->have_posts() ) : $video->the_post();
      ?>
      <section class="home-video" style="background-image: url('<?php the_post_thumbnail_url('full'); ?>')">
        <div class="wrapper">
          <h2 class="home-video-title title title-secondary"><?php the_title(); ?></h2>
          <p class="home-video-text"><?php echo get_field('texto'); ?></p>
          <?php
          preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', get_field('link'), $match);
          $youtube_id = $match[1];
          ?>
          <a href="#" class="home-video-btn" data-url="<?php echo $youtube_id; ?>"><img src="<?php echo TPATH; ?>/images/play.png" alt=""></a>
        </div>

        <div class="video-modal">
          <div class="video-modal-bg"></div>
          <a href="#" class="video-modal-close"><img src="<?php echo TPATH; ?>/images/menu-close.png" alt=""></a>

          <div class="wrapper">
            <div class="videoWrapper">
              <iframe src="" allowfullscreen></iframe>
            </div>
          </div>
        </div>
      </section>
      <?php
    endwhile;
    wp_reset_postdata();
  endif;
  ?>

  <section class="home-contact">
    <div class="wrapper">
      <?php
      $infos = new WP_Query('pagename=home');
      if ( $infos->have_posts() ) :
        $infos->the_post();
        ?>
        <p class="home-contact-text title title-secondary">
        <?php echo get_field('home_contact_text')?>
        <a href="mailto:<?php echo get_field('home_contact_email')?>"> <?php echo get_field('home_contact_email')?></a>
        </p>
        <?php
      endif;
      wp_reset_query();
      ?>
    </div>
  </section>

<?php include '_footer.php'; ?>
