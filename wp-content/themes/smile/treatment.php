<?php
/*
  Template Name: Tratamento
*/
?>

<?php include '_header.php'; ?>

  <section class="internal-banner" style="background-image: url(<?php echo TPATH; ?>/images/treatment-bg.png)"></section>

  <main class="treatment">
    <div class="wrapper">
      <span class="section-name"><?php echo get_field('treatment_tag'); ?></span>

      <div class="treatment-text">
        <?php
        while ( have_posts() ) : the_post();
          the_content();
        endwhile;
        ?>

      </div>
    </div>
  </main>

<?php include '_footer.php'; ?>
