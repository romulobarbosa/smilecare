<?php
/*
  Template Name: Quem Somos
*/
?>

<?php include '_header.php'; ?>

  <section class="internal-banner" style="background-image: url(<?php echo TPATH; ?>/images/about-bg.png)"></section>

  <main>
    <div class="wrapper wrapper-about">

      <?php
      while ( have_posts() ) : the_post(); ?>
        <section class="about-column about">
          <div class="about-column-content">
            <span class="section-name"><?php echo get_field('about_tag');?></span>
            <h2 class="title title-secondary about-title"><?php the_title(); ?></h2>
            <?php the_content(); ?>
          </div>
        </section>

        <section class="about-column mission">
          <div class="about-column-content">
            <span class="section-name"><?php echo get_field('mission_tag');?></span>
            <h2 class="title title-secondary about-title about-title-mission"><?= __('Mission', 'Smilecare'); ?></h2>
            <?php echo get_field('mission') ?>
          </div>
        </section>
        <?php
      endwhile;
      wp_reset_query();
      ?>
    </div>

    <?php
    $team = new WP_Query(array('post_type'=>'team', 'post_status'=>'publish', 'posts_per_page' => '-1'));
    if ( $team->have_posts() ) :
      ?>
      <section class="team">
        <div class="wrapper">
          <h2 class="title title-secondary team-title"><?= __('Team', 'Smilecare'); ?></h2>
          <div class="team-list">
            <?php
            while ( $team->have_posts() ) : $team->the_post();
              ?>
              <div class="team-member">
                <div class="team-member-avatar"><img src="<?php echo get_field('avatar'); ?>" alt=""></div>
                <span class="team-member-name"><?php the_title(); ?></span>
                <span class="team-member-position"><?php echo get_field('cargo') ?></span>
              </div>
              <?php
            endwhile;
            ?>

          </div>
        </div>
      </section>
      <?php
      wp_reset_postdata();
    endif;
    ?>
  </main>

<?php include '_footer.php'; ?>
