jQuery(document).ready(function( $ ) {
  var menu = $( '#navigation-panel' );
  var menuButton = $( '#js-menu-toggle' );
  
  var search = $( '#header-search-container' );
  var searchButton = $( '#js-search-toggle' );
  
  menuButton.on( 'click', function( e ) {
    e.preventDefault();
    if ( searchButton.hasClass( 'active' ) ) {
      search.slideToggle( 'fast' );
      searchButton.toggleClass( 'active' );
    }
    menu.slideToggle( 'fast' );
    menuButton.toggleClass( 'active' );
  });
  
  searchButton.on( 'click', function( f ) {
    f.preventDefault();
    if ( menuButton.hasClass( 'active' ) ) {
      menu.slideToggle( 'fast' );
      menuButton.toggleClass( 'active' );
    }
    search.slideToggle( 'fast', function() {
      searchButton.toggleClass( 'active' ); 
    });
    search.find( '.search-field' ).focus();
  });
  
  $( '#site-navigation .page_item_has_children > a, #site-navigation .menu-item-has-children > a' ).after( '<button class="js-child-toggle"></button>' );
  
  $( '.js-child-toggle' ).on( 'click', function( g ) {
    var _this = $( this );
    g.preventDefault();
    _this.toggleClass( 'toggled-on' );
    _this.next( '.children, .sub-menu' ).toggleClass( 'on' );
  });
});