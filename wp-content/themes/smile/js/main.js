$(document).ready(function(){
  "use strict";

  function animate(){
    $('.animate').each(function(){

      var posItem;
      if ($(window).width() > 767) {
        posItem = $(this).offset().top + 200;
      } else {
        posItem = $(this).offset().top + 50;
      }

      if(posItem > $(window).scrollTop() && posItem < $(window).scrollTop() + $(window).height()){
        $(this).addClass('active');
        $(this).addClass($(this).data("animate"));
      }
    });
    $('.animate-now').each(function(){
      var posItem = $(this).offset().top;

      if(posItem > $(window).scrollTop() && posItem < $(window).scrollTop() + $(window).height()){
        $(this).addClass('active');
        $(this).addClass($(this).data("animate"));
      }
    });
  }

  // Fire animations
  animate();
  $(window).on('scroll', function(){
    animate();
  });

  // Menu
  $(document).on('click', '.menu-btn', function(e){
    e.preventDefault();

    $('.menu-list').toggleClass('active');
  })

  // Banners header
  $('.banner-container').owlCarousel({
    loop:true,
    margin:0,
    nav:false,
    items: 1,
    autoplay: true,
    smartSpeed: 400,
    autoplayTimeout: 8000
})

  // Video
  $(document).on('click', '.home-video-btn', function(e){
    e.preventDefault();
    const videoID = $(this).attr('data-url')
    const embedULR = 'https://www.youtube.com/embed/' + videoID + '?rel=0'

    $('.videoWrapper iframe').attr('src', embedULR)

    $('.video-modal').addClass('active')
  })

  $(document).on('click', '.video-modal-close, .video-modal-bg', function(e){
    e.preventDefault();

    const embedULR = $('.videoWrapper iframe').attr('src')

    $('.video-modal').removeClass('active')
    $('.videoWrapper iframe').attr('src', '')
  })

});

